import tkinter as tk
from yolo import YOLO
from PIL import Image,ImageTk
import cv2
import threading
from gtts import gTTS
import tempfile
from pygame import mixer
from queue import Queue
import time
from timeit import default_timer as timer


def get_display_class_name(cls):
    if cls==1:
        return '寶特瓶'
    if cls==2:
        return '飲料紙杯'
    if cls==3:
        return '便當盒'

    return ''

def get_say_class_text(cls_count_array):

    size = len(cls_count_array)
    if size==1:
        return "{}".format(cls_count_array[0])
    elif size==2:
        return "{},{}".format(cls_count_array[0],cls_count_array[1])
    elif size==3:
        return "{},{},{}".format(cls_count_array[0],cls_count_array[1],cls_count_array[2])

def reset_label():
    cls_1.configure(image=rc001_off_img)
    cls_1.image = rc001_off_img
    cls_2.configure(image=rc002_off_img)
    cls_2.image = rc002_off_img
    cls_3.configure(image=rc003_off_img)
    cls_3.image = rc003_off_img

def speech_class(classes):
    cls_temp=[]
    cls_1_count=0
    cls_2_count=0
    cls_3_count=0

    for cls in classes:
        if cls==1:
            cls_1_count+=1
        if cls==2:
            cls_2_count+=1
        if cls==3:
            cls_3_count+=1

    if cls_1_count>0:
        cls_temp.append("{}個{}".format(cls_1_count,get_display_class_name(1)))
    if cls_2_count > 0:
        cls_temp.append("{}個{}".format(cls_2_count,get_display_class_name(2)))
    if cls_3_count > 0:
        cls_temp.append("{}個{}".format(cls_3_count,get_display_class_name(3)))

    if len(cls_temp)==0:
        return
    my_variable = get_say_class_text(cls_temp)
    q.queue.clear()
    q.put(my_variable)
    #say(my_variable)


def say(text, filename=None):
    with tempfile.NamedTemporaryFile(delete=True) as temp:
        tts = gTTS(text, lang='zh-tw', slow=False)
        if filename is None:
            filename = "{}.mp3".format(temp.name)
        tts.save(filename)
        mixer.init()
        mixer.music.load(filename)
        mixer.music.play()
        while mixer.music.get_busy() == True:
            continue
        mixer.quit()

def upate_label(classes):
    reset_label()
    for cls in classes:
        if cls==1:
            img1=rc001_on_img
            cls_1.configure(image=img1)
            cls_1.image = img1
        elif cls==2:
            img2=rc002_on_img
            cls_2.configure(image=img2)
            cls_2.image = img2
        elif cls==3:
            img3 = rc003_on_img
            cls_3.configure(image=img3)
            cls_3.image = img3


def detect_video_frame(vid,yolo):

    return_value, frame = vid.read()
    frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGBA)
    image = Image.fromarray(frame)
    boxes, scores, classes, image = yolo.detect_both_image_boxes(image)
    update_camera_preview(image)
    #if len(classes)==0:

    upate_label(classes)
    speech_class(classes)

def diaplay_frame(vid):
    return_value, frame = vid.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    image = Image.fromarray(frame)
    return image

def update_camera_preview(img_frame):
    img_show = ImageTk.PhotoImage(image=img_frame)
    live.configure(image=img_show)
    live.image = img_show

def thread_job(vid):
    yolo = YOLO()
    while True:
        detect_video_frame(vid,yolo)
        time.sleep(0.2)


def thread_say_job():
    while True:
        print("Queue count:",q.qsize())
        text=q.get()
        try:
            start = timer()
            say(text)
            end = timer()
            print("Say : ", text," (",end - start,")")
        except:
            print("gTTs error!")
            continue




def do_detect_thread():
    detect_job = threading.Thread(target=thread_job, args=(vid,))
    detect_job.start()
    say_job = threading.Thread(target=thread_say_job)
    say_job.start()


if __name__ == '__main__':

    vid = cv2.VideoCapture(0)
    if not vid.isOpened():
        raise IOError("Couldn't open webcam or video")
        #yolo.close_session()
        exit()

    q = Queue(maxsize=2)

    root = tk.Tk()
    root.title('AI @ CTO')
    my_frame_1 = tk.Frame(root, bd=2, relief=tk.SUNKEN)
    my_frame_1.pack(side=tk.TOP)
    my_frame_2 = tk.Frame(root, bd=2, relief=tk.GROOVE)
    my_frame_2.pack(side=tk.BOTTOM)

    live = tk.Label(my_frame_1)
    live.pack()

    rc001_off_img = ImageTk.PhotoImage(image=Image.open('images/rc001_off.png', 'r').resize((250,250),Image.ANTIALIAS))
    rc002_off_img = ImageTk.PhotoImage(image=Image.open('images/rc002_off.png', 'r').resize((250, 250), Image.ANTIALIAS))
    rc003_off_img = ImageTk.PhotoImage(image=Image.open('images/rc003_off.png', 'r').resize((250, 250), Image.ANTIALIAS))

    rc001_on_img = ImageTk.PhotoImage(image=Image.open('images/rc001_on.png', 'r').resize((250, 250), Image.ANTIALIAS))
    rc002_on_img = ImageTk.PhotoImage(image=Image.open('images/rc002_on.png', 'r').resize((250, 250), Image.ANTIALIAS))
    rc003_on_img = ImageTk.PhotoImage(image=Image.open('images/rc003_on.png', 'r').resize((250, 250), Image.ANTIALIAS))

    is_cls_1=False
    is_cls_2=False
    is_cls_3=False

    cls_1 = tk.Label(my_frame_2, height=250, width=250, image=rc001_off_img)
    cls_1.pack(side=tk.LEFT)
    cls_2 = tk.Label(my_frame_2, height=250, width=250, image=rc002_off_img)
    cls_2.pack(side=tk.LEFT)
    cls_3 = tk.Label(my_frame_2, height=250, width=250, image=rc003_off_img)
    cls_3.pack(side=tk.LEFT)

    #root.after(1000, update_camera_preview)
    root.after(1000, do_detect_thread)
    root.mainloop()

