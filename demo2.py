import tkinter as tk
from yolo import YOLO
from PIL import Image,ImageTk
import cv2
import threading
from gtts import gTTS
from io import BytesIO
import tempfile
from pygame import mixer


def get_display_class_name(cls):
    if cls==1:
        return '寶特瓶'
    if cls==2:
        return '飲料紙杯'
    if cls==3:
        return '便當盒'

    return ''

def reset_label():
    cls_1.configure(image=rc001_off_img)
    cls_1.image = rc001_off_img
    cls_2.configure(image=rc002_off_img)
    cls_2.image = rc002_off_img
    cls_3.configure(image=rc003_off_img)
    cls_3.image = rc003_off_img

def speech_class(cls):
    if cls==0:
        return
    if cls==1 and is_cls_1:
        return
    if cls==2 and is_cls_2:
        return
    if cls==3 and is_cls_3:
        return

    my_variable = get_display_class_name(cls)
    say(my_variable)

def say(text, filename=None):
    with tempfile.NamedTemporaryFile(delete=True) as temp:
        tts = gTTS(text, lang='zh-tw', slow=False)
        if filename is None:
            filename = "{}.mp3".format(temp.name)
        tts.save(filename)
        mixer.init()
        mixer.music.load(filename)
        mixer.music.play()
        while mixer.music.get_busy() == True:
            continue
        mixer.quit()

def upate_label(cls, score):
    speech_class(cls)
    is_cls_1=True if cls==1 else False
    is_cls_2=True if cls==2 else False
    is_cls_3=True if cls==3 else False
    img1=rc001_on_img if cls==1 else rc001_off_img
    cls_1.configure(image=img1)
    cls_1.image = img1

    img2=rc002_on_img if cls==2 else rc002_off_img
    cls_2.configure(image=img2)
    cls_2.image = img2

    img3 = rc003_on_img if cls == 3 else rc003_off_img
    cls_3.configure(image=img3)
    cls_3.image = img3


def detect_video_frame(vid,yolo):

    return_value, frame = vid.read()
    frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGBA)
    image = Image.fromarray(frame)
    boxes, scores, classes = yolo.detect_image_boxes(image)
    if len(classes)==0:
        reset_label()
    for i in range(len(classes)):
        score = scores[i]
        cls = classes[i]
        upate_label(cls,score)

def diaplay_frame(vid):
    return_value, frame = vid.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    image = Image.fromarray(frame)
    return image

def update_camera_preview():
    img_frame = diaplay_frame(vid)
    img_show = ImageTk.PhotoImage(image=img_frame)
    live.configure(image=img_show)
    live.image = img_show
    root.after(100, update_camera_preview)

def thread_job(vid):
    yolo = YOLO()
    while True:
        detect_video_frame(vid,yolo)


def do_detect_thread():
    detect_job = threading.Thread(target=thread_job, args=(vid,))
    detect_job.start()


if __name__ == '__main__':

    vid = cv2.VideoCapture(0)
    if not vid.isOpened():
        raise IOError("Couldn't open webcam or video")
        #yolo.close_session()
        exit()

    root = tk.Tk()
    root.title('AI @ CTO')
    my_frame_1 = tk.Frame(root, bd=2, relief=tk.SUNKEN)
    my_frame_1.pack(side=tk.TOP)
    my_frame_2 = tk.Frame(root, bd=2, relief=tk.GROOVE)
    my_frame_2.pack(side=tk.BOTTOM)

    live = tk.Label(my_frame_1)
    live.pack()

    rc001_off_img = ImageTk.PhotoImage(image=Image.open('images/rc001_off.png', 'r').resize((250,250),Image.ANTIALIAS))
    rc002_off_img = ImageTk.PhotoImage(image=Image.open('images/rc002_off.png', 'r').resize((250, 250), Image.ANTIALIAS))
    rc003_off_img = ImageTk.PhotoImage(image=Image.open('images/rc003_off.png', 'r').resize((250, 250), Image.ANTIALIAS))

    rc001_on_img = ImageTk.PhotoImage(image=Image.open('images/rc001_on.png', 'r').resize((250, 250), Image.ANTIALIAS))
    rc002_on_img = ImageTk.PhotoImage(image=Image.open('images/rc002_on.png', 'r').resize((250, 250), Image.ANTIALIAS))
    rc003_on_img = ImageTk.PhotoImage(image=Image.open('images/rc003_on.png', 'r').resize((250, 250), Image.ANTIALIAS))

    is_cls_1=False
    is_cls_2=False
    is_cls_3=False

    cls_1 = tk.Label(my_frame_2, height=250, width=250, image=rc001_off_img)
    cls_1.pack(side=tk.LEFT)
    cls_2 = tk.Label(my_frame_2, height=250, width=250, image=rc002_off_img)
    cls_2.pack(side=tk.LEFT)
    cls_3 = tk.Label(my_frame_2, height=250, width=250, image=rc003_off_img)
    cls_3.pack(side=tk.LEFT)

    root.after(1000, update_camera_preview)
    root.after(1000, do_detect_thread)
    root.mainloop()

